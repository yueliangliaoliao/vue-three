import * as THREE from "three";
import { RGBELoader } from "three/examples/jsm/loaders/RGBELoader"; //rebe加载器
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"; //倒入控制器，轨道控制器
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

class Base3D {
  constructor(selector, onFinish) {
    this.container = document.querySelector(selector);
    this.camera;
    this.scene;
    this.renderer;
    this.model;
    this.dish;
    this.mixer;
    this.animateAction;
    this.clock = new THREE.Clock();
    this.onFinish = onFinish;
    this.init();
    this.animate();
    this.progressFn;
  }
  onProgress(fn) {
    this.progressFn = fn;
  }
  init() {
    // 初始化场景
    this.initScene();
    // 初始化相机
    this.initCamera();
    // 初始化渲染器
    this.initRenderer();
    // 控制器
    // this.initControls();
    // 添加物品
    this.addMesh();
    // 监听场景大小改变，调整渲染尺寸
    window.addEventListener("resize", this.onWindowResize.bind(this));
    // 监听滚轮事件
    window.addEventListener("mousewheel", this.onMouseWheel.bind(this));
  }
  // 初始化场景
  initScene() {
    this.scene = new THREE.Scene();
    this.setEnvMap("000");
  }
  // 初始化相机
  initCamera() {
    this.camera = new THREE.PerspectiveCamera(
      45, // 角度
      window.innerWidth / window.innerHeight, // 比例
      0.25, // 近
      200 // 远
    );
    // 相机位置
    this.camera.position.set(-1.8, 0.6, 2.7);
  }
  initRenderer() {
    this.renderer = new THREE.WebGLRenderer({
      antialias: true, // 抗锯齿
    });
    // 设置像素比
    this.renderer.setPixelRatio(window.devicePixelRatio);
    // 设置渲染尺寸大小
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    // 色调映射
    this.renderer.toneMapping = THREE.ACESFilmicToneMapping;
    // 曝光率
    this.renderer.toneMappingExposure = 2;
    // 追加到容器上
    this.container.appendChild(this.renderer.domElement);
  }
  // 场景设置
  setEnvMap(hdr) {
    new RGBELoader().setPath("./hdr/").load(`${hdr}.hdr`, (texture) => {
      texture.mapping = THREE.EquirectangularRefractionMapping;
      this.scene.background = texture;
      this.scene.environment = texture;
    });
  }
  render() {
    var delta = this.clock.getDelta();
    this.mixer && this.mixer.update(delta);
    this.renderer.render(this.scene, this.camera);
  }
  animate() {
    this.renderer.setAnimationLoop(this.render.bind(this));
  }
  // 控制器
  initControls() {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
  }
  // 设置模型
  setModel(modelName) {
    return new Promise((resolve, reject) => {
      const loader = new GLTFLoader().setPath("./gltf/");
      loader.load(
        modelName,
        (gltf) => {
          console.log(gltf);
          this.model && this.model.removeFromParent();
          this.model = gltf.scene.children[0];
          if (modelName === "bag2.glb" && !this.dish) {
            this.dish = gltf.scene.children[5];
            // 修改相机为模型相机
            this.camera = gltf.cameras[0];
            // 调用动画
            this.mixer = new THREE.AnimationMixer(gltf.scene.children[1]);
            this.animateAction = this.mixer.clipAction(gltf.animations[0]);
            // 设置动画播放时长
            this.animateAction.setDuration(20).setLoop(THREE.LoopOnce);
            // 设置播放后停止
            this.animateAction.clampWhenFinished = true;
            //   设置灯光
            this.spotlight1 = gltf.scene.children[2].children[0];
            this.spotlight1.intensity = 1;
            this.spotlight2 = gltf.scene.children[3].children[0];
            this.spotlight2.intensity = 1;
            this.spotlight3 = gltf.scene.children[4].children[0];
            this.spotlight3.intensity = 1;

            // this.scene.add(this.dish);
          }
          this.scene.add(gltf.scene);
          resolve(`${this.modelName}模型添加成功`);
        },
        (e) => {
          //   console.log("模型加载进度");
          //   console.log(e);
          this.progressFn(e);
        }
      );
    });
  }
  // 添加物品
  async addMesh() {
    let res = await this.setModel("bag2.glb");
    this.onFinish(res);
  }
  // 监听尺寸
  onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }
  // 监听滚轮事件
  onMouseWheel(e) {
    let timeScale = e.deltaY > 0 ? 1 : -1;
    this.animateAction.setEffectiveTimeScale(timeScale);
    this.animateAction.paused = false;
    this.animateAction.play();
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
    this.timeoutId = setTimeout(() => {
      this.animateAction.halt(0.3);
    }, 300);
  }
}
export default Base3D;
